import { isString } from 'min-dash';

import { Moddle as ModdleOrg } from 'moddle';
import Factory from './factory.js';
import Registry from './registry.js';

export default function Moddle(packages) {
  ModdleOrg.call(this, packages);
  this.factory = new Factory(this, this.properties);
  this.registry = new Registry(packages, this.properties);
}

Moddle.prototype = Object.create(ModdleOrg.prototype);

Moddle.prototype.getType = function(descriptor) {
  var cache = this.typeCache;
  var name = isString(descriptor) ? descriptor : descriptor.ns.name;
  var className = name.substr(name.indexOf(":") + 1);
  var type = cache[name];
  var idPropertyName = 'xmi:id';
  var localPropertyName = idPropertyName;

  if (!type) {
    descriptor = this.registry.getEffectiveDescriptor(name);
    if(descriptor.idProperty && descriptor.idProperty.name){
      localPropertyName = descriptor.idProperty.name;
    } 
    else {
        descriptor.idProperty = {
        name: idPropertyName,
        type: 'String',
        isId: true,
      };
    }

    var orgType = this.factory.createType(descriptor);
    class ModdleClass extends orgType {
      constructor(attrs) {
        super(attrs);
        if (attrs && attrs[idPropertyName]) {
          this.$id = attrs[idPropertyName];
          delete attrs[idPropertyName];
        }
        else if (attrs && attrs[localPropertyName]) {
          this.$id = attrs[localPropertyName];
        }
      }
    }
    var type = ModdleClass;
    Object.defineProperty(type, 'name', { value: className, writable: false });
    cache[name] = type;
  }

  return type;
};
