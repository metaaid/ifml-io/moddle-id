export {
  default as Moddle
} from './moddle.js';

export {
  parseNameNS
} from 'moddle';

export {
  isBuiltInType,
  isSimpleType,
  coerceType
} from 'moddle';