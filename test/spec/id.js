import expect from '../expect.js';

import {
  createModelBuilder
} from '../helper.js';


describe('moddle', function() {

  var createModel = createModelBuilder('test/fixtures/model/');

  describe('base', function() {

    var model = createModel([ 'properties', 'extension/uml' ]);


    it('no own id defined', function() {

      var expectedDescriptorProperties = [];
      var expectedDescriptorPropertiesByName = {};

      var ComplexType = model.getType('uml:Element');
      var descriptor = model.getElementDescriptor(ComplexType);
      expect(descriptor).to.exist;
      expect(descriptor.properties).to.jsonEqual(expectedDescriptorProperties);
      expect(descriptor.propertiesByName).to.jsonEqual(expectedDescriptorPropertiesByName);
      expect(descriptor.propertiesByName).to.jsonEqual(expectedDescriptorPropertiesByName);

      expect((new ComplexType({"xmi:id": "testId"})).$id).to.eql("testId");
    });

    it('inherit id', function() {

      var expectedDescriptorProperties = [{
        name: 'id',
        type: 'String',
        isAttr: true,
        isId: true,
        ns: {name: 'props:id',id: 'props:id', prefix: 'props', localName: 'id'},
        inherited: true,
      }];
      var expectedDescriptorPropertiesByName = {
        'id': {
          name: 'id',
          type: 'String',
          isAttr: true,
          isId: true,
          ns: { name: 'props:id', id: 'props:id', prefix: 'props', localName: 'id' },
          inherited: true
        },
        'props:id': {
          name: 'id',
          type: 'String',
          isAttr: true,
          isId: true,
          ns: { name: 'props:id', id: 'props:id', prefix: 'props', localName: 'id' },
          inherited: true,
        },
      };

      var ComplexType = model.getType('props:Complex');
      var descriptor = model.getElementDescriptor(ComplexType);
      expect(descriptor).to.exist;
      expect(descriptor.properties).to.jsonEqual(expectedDescriptorProperties);
      expect(descriptor.propertiesByName).to.jsonEqual(expectedDescriptorPropertiesByName);

      expect((new ComplexType({id: "test"})).$id).to.eq("test");
    });

    it('inherit id with xmi:id', function() {
  
      var ComplexType = model.getType('props:Complex');
  
      expect((new ComplexType({"xmi:id": "test"})).$id).to.eq("test");
    });

    it('populate $id to json', function() {
  
      var ComplexType = model.getType('props:Complex');
  
      expect(JSON.stringify((new ComplexType({"xmi:id": "test"})))).to.eq('{"$id":"test","$type":"props:Complex"}');
    });

    it('should update js className', function() {
      var type = model.getType('props:Complex');
      expect(type.name).to.eql('Complex');
    });
  });
});
