function parseParts(name, defaultPrefix) {
  var localName, prefix;
  var parts = name.split(/:/);

  // no prefix (i.e. only local name)
  if (parts.length === 1) {
    localName = name;
    prefix = defaultPrefix;
  }

  // prefix + local name
  else if (parts.length === 2) {
    localName = parts[1];
    prefix = parts[0];
  } else {
    throw new Error('expected <prefix:localName> or <localName>, got ' + name);
  }
  return { localName: localName, prefix: prefix };
}

/**
 * Parses a namespaced attribute name of the form (ns:)localName to an object,
 * given a default prefix to assume in case no explicit namespace is given.
 *
 * @param {String} name
 * @param {String} [defaultPrefix] the default prefix to take, if none is present.
 *
 * @return {Object} the parsed name
 */
export function parseName(name, defaultPrefix, id = null) {
  var parsedName = parseParts(name, defaultPrefix);

  name =
    (parsedName.prefix ? parsedName.prefix + ':' : '') + parsedName.localName;
  if (id) {
    id = (parsedName.prefix ? parsedName.prefix + ':' : '') + id;
  } else {
    id = name;
  }

  return {
    name: name,
    id: id,
    prefix: parsedName.prefix,
    localName: parsedName.localName,
  };
}
